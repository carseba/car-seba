﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface ICarRepository
    {
        IList<CarDto> GetAll();
        CarDto GetById(int id);
        void Add(Car car);
        void Update(int id, Car updatedCar);
        void Remove(int id);
        void Save();
    }
}
