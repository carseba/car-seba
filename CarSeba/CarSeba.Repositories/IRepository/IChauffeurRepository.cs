﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface IChauffeurRepository
    {
        IList<ChauffeurDto> GetAll();
        ChauffeurDto GetById(int id);
        void Add(Chauffeur carType);
        void Update(int id, Chauffeur updatedCarType);
        void Remove(int id);
        void Save();
    }
}
