﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface IRentRequest
    {
        IEnumerable<RentRequest> GetAll();
        void Create(RentRequest rentRequest);
    }
}
