﻿
using System.Collections.Generic;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface ICarTypeRepository
    {
        IList<CarTypeDto> GetAll();
        CarTypeDto GetById(int id);
        void Add(CarType carType);
        void Update(int id, CarType updatedCarType);
        void Remove(int id);
        void Save();
    }
}
