﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User GetById(int id);
        void Add(User user);
        void Update(int id, User updatedUser);
        void Remove(int id);
        void Save();
    }
}
