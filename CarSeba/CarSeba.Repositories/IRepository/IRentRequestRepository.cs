﻿using System.Collections.Generic;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;

namespace CarSeba.Repositories.IRepository
{
    public interface IRentRequestRepository
    {
        IList<RentRequestDto> GetAll();
        RentRequestDto GetById(int id);
        void Add(RentRequest rentRequest);
        void Update(int id, RentRequest updatedrRentRequest);
        void Remove(int id);
        void Save();
    }
}
