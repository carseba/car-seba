﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarSeba.Models;
using CarSeba.Models.Entities;
using CarSeba.Repositories.IRepository;

namespace CarSeba.Repositories.Repository
{
    public class UserRepository : IUserRepository
    {
        public CarSebaDbContext Context { get; set; }

        public UserRepository()
        {
            Context = new CarSebaDbContext();
        }
        public IEnumerable<User> GetAll()
        {

            var response = Context.Users.ToList();
            return response;
        }

        public User GetById(int id)
        {
            var response = Context.Users.Find(id);
            return response;
        }

        public void Add(User user)
        {
            Context.Users.Add(user);
            Save();
        }

        public void Update(int id, User updatedUser)
        {
            var existing = Context.Users.SingleOrDefault<User>(user => user.Id == id);

            if (existing != null)
            {
                existing.UserName = updatedUser.UserName;
                existing.Password = updatedUser.Password;
                existing.UserType = updatedUser.UserType;
            }

            Context.Users.Attach(existing);
            Context.Entry(existing).State = EntityState.Modified;
            Save();
        }

        public void Remove(int id)
        {
            User existing = Context.Users.Find(id);
            Context.Users.Remove(existing);
            Save();
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
