﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.IRepository;

namespace CarSeba.Repositories.Repository
{
    public class ChauffeurRepository : IChauffeurRepository
    {
        public CarSebaDbContext Context { get; set; }

        public ChauffeurRepository()
        {
            Context = new CarSebaDbContext();
        }
        public IList<ChauffeurDto> GetAll()
        {
            return Context.Chauffeurs.Select(p => new ChauffeurDto()
            {
                Id = p.Id,
                Name = p.Name,
                LicenseNum = p.LicenseNum,
                Status = p.Status,
                RentRequests = p.RentRequests.ToList()
            }).ToList();
        }

        public ChauffeurDto GetById(int id)
        {
            return Context.Chauffeurs.Select(p => new ChauffeurDto()
            {
                Id = p.Id,
                Name = p.Name,
                LicenseNum = p.LicenseNum,
                Status = p.Status,
                RentRequests = p.RentRequests.ToList()
            }).FirstOrDefault(p => p.Id == id);
        }

        public void Add(Chauffeur chauffeur)
        {
            Context.Chauffeurs.Add(chauffeur);
            Save();
        }

        public void Update(int id, Chauffeur updatedChauffeur)
        {
            var existing = Context.Chauffeurs.SingleOrDefault<Chauffeur>(carType => carType.Id == id);

            if (existing != null)
            {
                existing.Name = updatedChauffeur.Name;
                existing.LicenseNum = updatedChauffeur.LicenseNum;
                existing.Status = updatedChauffeur.Status;

            }

            Context.Chauffeurs.Attach(existing);
            Context.Entry(existing).State = EntityState.Modified;
            Save();
        }

        public void Remove(int id)
        {
            Chauffeur existing = Context.Chauffeurs.Find(id);
            Context.Chauffeurs.Remove(existing);
            Save();
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
