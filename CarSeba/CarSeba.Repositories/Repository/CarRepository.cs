﻿

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarSeba.Models;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.IRepository;

namespace CarSeba.Repositories.Repository
{
    public class CarRepository : ICarRepository
    {
        public CarSebaDbContext Context { get; set; }

        public CarRepository()
        {
            Context = new CarSebaDbContext();
        }
        public IList<CarDto> GetAll()
        {
            return Context.Cars.Select(p => new CarDto()
            {
                Id = p.Id,
                RegistrationNo = p.RegistrationNo,
                ModelName = p.ModelName,
                BrandName = p.BrandName,
                CarTypeId = p.CarTypeId,
                RentRequests = p.RentRequests.ToList()
            }).ToList();
        }

        public CarDto GetById(int id)
        {
            return Context.Cars.Select(p => new CarDto()
            {
                Id = p.Id,
                RegistrationNo = p.RegistrationNo,
                ModelName = p.ModelName,
                BrandName = p.BrandName,
                CarTypeId = p.CarTypeId,
                RentRequests = p.RentRequests.ToList()
            }).FirstOrDefault(p => p.Id == id);
        }

        public void Add(Car car)
        {
            Context.Cars.Add(car);
            Save();
        }

        public void Update(int id, Car updatedCar)
        {
            var existing = Context.Cars.SingleOrDefault<Car>(car => car.Id == id);

            if (existing != null)
            {
                existing.RegistrationNo = updatedCar.RegistrationNo;
                existing.ModelName = updatedCar.ModelName;
                existing.BrandName = updatedCar.BrandName;
                existing.CarTypeId = updatedCar.CarTypeId;
            }

            Context.Cars.Attach(existing);
            Context.Entry(existing).State = EntityState.Modified;
            Save();
        }

        public void Remove(int id)
        {
            Car existing = Context.Cars.Find(id);
            Context.Cars.Remove(existing);
            Save();
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
