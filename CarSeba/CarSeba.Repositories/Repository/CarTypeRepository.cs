﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CarSeba.Models;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.IRepository;

namespace CarSeba.Repositories.Repository
{
    public class CarTypeRepository : ICarTypeRepository
    {
        public CarSebaDbContext Context { get; set; }

        public CarTypeRepository()
        {
            Context = new CarSebaDbContext();
        }
        public IList<CarTypeDto> GetAll()
        {
            //return Context.CarTypes.Select(p => new CarTypeDto()
            //{
            //    Id = p.Id,
            //    Name = p.Name,
            //    //Cars = p.Cars.ToList()
            //}).ToList();
            var temp = Context.CarTypes.Include(p => p.Cars).ToList();
            IList<CarTypeDto> carTypeList = new List<CarTypeDto>();

            foreach (var carType in temp)
            {
               carTypeList.Add
               (
                   new CarTypeDto()
                  {
                    Id = carType.Id,
                    Name = carType.Name,
                    Cars = carType.Cars
                  }
               ); 
            }
            return carTypeList;
        }

        public CarTypeDto GetById(int id)
        {
            
            return Context.CarTypes.Select(p => new CarTypeDto()
            {
                Id = p.Id,
                Name = p.Name,
                //Cars = p.Cars.ToList()
            }).FirstOrDefault(p => p.Id == id);
        }

        public void Add(CarType carType)
        {
            Context.CarTypes.Add(carType);
            Save();
        }

        public void Update(int id, CarType updatedCarType)
        {
            var existing = Context.CarTypes.SingleOrDefault<CarType>(carType => carType.Id == id);

            if (existing != null)
            {
                existing.Name = updatedCarType.Name;

            }

            Context.CarTypes.Attach(existing);
            Context.Entry(existing).State = EntityState.Modified;
            Save();
        }

        public void Remove(int id)
        {
            CarType existing = Context.CarTypes.Find(id);
            Context.CarTypes.Remove(existing);
            Save();
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
