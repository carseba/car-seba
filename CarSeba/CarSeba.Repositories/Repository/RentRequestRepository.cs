﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.IRepository;

namespace CarSeba.Repositories.Repository
{
    public class RentRequestRepository : IRentRequestRepository
    {
        public CarSebaDbContext Context { get; set; }
        public RentRequestRepository()
        {
            Context = new CarSebaDbContext();
        }

        public IList<RentRequestDto> GetAll()
        {
            return Context.RentRequests.Select(p => new RentRequestDto()
            {
                Id = p.Id,
                CustomerName = p.CustomerName,
                ContactNumber = p.ContactNumber,
                PickUpFrom = p.PickUpFrom,
                PickUpDate = p.PickUpDate,
                PickUpTime = p.PickUpTime,
                DropOffTo = p.DropOffTo,
                DropOffDate = p.DropOffDate,
                DropOffTime = p.DropOffTime,
                ServiceStatus = p.ServiceStatus,
                Fare = p.Fare,
                ReservedCarId = p.ReservedCarId,
                ChauffeurId = p.ChauffeurId
                
            }).ToList();
        }

        public RentRequestDto GetById(int id)
        {
            return Context.RentRequests.Select(p => new RentRequestDto()
            {
                Id = p.Id,
                CustomerName = p.CustomerName,
                ContactNumber = p.ContactNumber,
                PickUpFrom = p.PickUpFrom,
                PickUpDate = p.PickUpDate,
                PickUpTime = p.PickUpTime,
                DropOffTo = p.DropOffTo,
                DropOffDate = p.DropOffDate,
                DropOffTime = p.DropOffTime,
                ServiceStatus = p.ServiceStatus,
                Fare = p.Fare,
                ReservedCarId = p.ReservedCarId,
                ChauffeurId = p.ChauffeurId
            }).FirstOrDefault(p => p.Id == id);
        }

        public void Add(RentRequest rentRequest)
        {
            Context.RentRequests.Add(rentRequest);
            Save();
        }

        public void Update(int id, RentRequest updatedrRentRequest)
        {
            var existing = Context.RentRequests.SingleOrDefault<RentRequest>(rentRequest => rentRequest.Id == id);

            if (existing != null)
            {
                existing.CustomerName = updatedrRentRequest.CustomerName;
                existing.PickUpFrom = updatedrRentRequest.PickUpFrom;
                existing.PickUpDate = updatedrRentRequest.PickUpDate;
                existing.PickUpTime = updatedrRentRequest.PickUpTime;
                existing.DropOffTo = updatedrRentRequest.DropOffTo;
                existing.DropOffDate = updatedrRentRequest.DropOffDate;
                existing.DropOffTime = updatedrRentRequest.DropOffTime;
                existing.ServiceStatus = updatedrRentRequest.ServiceStatus;
                existing.Fare = updatedrRentRequest.Fare;
                existing.ReservedCarId = updatedrRentRequest.ReservedCarId;
                existing.ChauffeurId = updatedrRentRequest.ChauffeurId;

            }

            Context.RentRequests.Attach(existing);
            Context.Entry(existing).State = EntityState.Modified;
            Save();
        }

        public void Remove(int id)
        {
            RentRequest existing = Context.RentRequests.Find(id);
            Context.RentRequests.Remove(existing);
            Save();
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
