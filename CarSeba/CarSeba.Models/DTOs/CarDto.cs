﻿
using System.Collections.Generic;
using CarSeba.Models.Entities;

namespace CarSeba.Models.DTOs
{
    public class CarDto
    {
        public int Id { get; set; }
        public string RegistrationNo { get; set; }
        public string ModelName { get; set; }
        public string BrandName { get; set; }
        public int CarTypeId { get; set; }
        public virtual List<RentRequest> RentRequests { get; set; }
                
    }
}
