﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSeba.Models.DTOs
{
    public class RentRequestDto
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public int ContactNumber { get; set; }
        public string PickUpFrom { get; set; }
        public DateTime PickUpDate { get; set; }
        public DateTime PickUpTime { get; set; }
        public string DropOffTo { get; set; }
        public DateTime DropOffDate { get; set; }
        public DateTime DropOffTime { get; set; }
        public string ServiceStatus { get; set; }
        public int? Fare { get; set; }
        public int? ReservedCarId { get; set; }
        public int? ChauffeurId { get; set; }
    }
}
