﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.Entities;

namespace CarSeba.Models.DTOs
{
    public class ChauffeurDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LicenseNum { get; set; }
        public string Status { get; set; }
        public virtual List<RentRequest> RentRequests { get; set; }
    }
}
