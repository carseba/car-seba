﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarSeba.Models.Entities
{
    public class Chauffeur
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string LicenseNum { get; set; }
        public string Status { get; set; }
        public virtual ICollection<RentRequest> RentRequests { get; set; }
    }
}
