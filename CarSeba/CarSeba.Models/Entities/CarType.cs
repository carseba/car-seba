﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CarSeba.Models.DTOs;

namespace CarSeba.Models.Entities
{
    public class CarType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Car> Cars { get; set; } 
    }
}
