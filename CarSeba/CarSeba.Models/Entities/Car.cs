﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSeba.Models.Entities
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        public string RegistrationNo { get; set; }
        public string ModelName { get; set; }
        public string BrandName { get; set; }
        public int CarTypeId { get; set; }

        [ForeignKey("CarTypeId")]
        public CarType CarType { get; set; }
        public virtual ICollection<RentRequest> RentRequests { get; set; }
    }
}
