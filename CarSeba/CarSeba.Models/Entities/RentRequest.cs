﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarSeba.Models.Entities
{
    public class RentRequest
    {
        [Key]
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public int ContactNumber { get; set; }
        public string PickUpFrom { get; set; }
        public DateTime PickUpDate { get; set; }
        public DateTime PickUpTime { get; set; }
        public string DropOffTo { get; set; }
        public DateTime DropOffDate { get; set; }
        public DateTime DropOffTime { get; set; }
        public string ServiceStatus { get; set; }
        public int? Fare { get; set; }
        public int? ReservedCarId { get; set; }
        public int? ChauffeurId { get; set; }
        [ForeignKey("ReservedCarId")]
        public virtual Car Car { get; set; }
        [ForeignKey("ChauffeurId")]
        public virtual Chauffeur Chauffeur { get; set; }
        
    }
}
