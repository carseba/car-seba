﻿using System.ComponentModel.DataAnnotations;

namespace CarSeba.Models.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
    }
}
