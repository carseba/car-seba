﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarSeba.Models.Entities;

namespace CarSeba.Models
{
    public class CarSebaDbContext : DbContext
    {
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Chauffeur> Chauffeurs { get; set; } 
        public DbSet<RentRequest> RentRequests { get; set; }
        public DbSet<User> Users { get; set; } 
        public CarSebaDbContext()
            :base("Chalo")
        {
            
        }
    }
}
