namespace CarSeba.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbDone : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RegistrationNo = c.String(),
                        ModelName = c.String(),
                        BrandName = c.String(),
                        CarTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarTypes", t => t.CarTypeId, cascadeDelete: true)
                .Index(t => t.CarTypeId);
            
            CreateTable(
                "dbo.CarTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Chauffeurs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LicenseNum = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.RentRequests", "CustomerName", c => c.String());
            AlterColumn("dbo.RentRequests", "PickUpFrom", c => c.String());
            AlterColumn("dbo.RentRequests", "DropOffTo", c => c.String());
            CreateIndex("dbo.RentRequests", "ReservedCarId");
            CreateIndex("dbo.RentRequests", "ChauffeurId");
            AddForeignKey("dbo.RentRequests", "ReservedCarId", "dbo.Cars", "Id");
            AddForeignKey("dbo.RentRequests", "ChauffeurId", "dbo.Chauffeurs", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RentRequests", "ChauffeurId", "dbo.Chauffeurs");
            DropForeignKey("dbo.RentRequests", "ReservedCarId", "dbo.Cars");
            DropForeignKey("dbo.Cars", "CarTypeId", "dbo.CarTypes");
            DropIndex("dbo.RentRequests", new[] { "ChauffeurId" });
            DropIndex("dbo.RentRequests", new[] { "ReservedCarId" });
            DropIndex("dbo.Cars", new[] { "CarTypeId" });
            AlterColumn("dbo.RentRequests", "DropOffTo", c => c.String(nullable: false));
            AlterColumn("dbo.RentRequests", "PickUpFrom", c => c.String(nullable: false));
            AlterColumn("dbo.RentRequests", "CustomerName", c => c.String(nullable: false));
            DropTable("dbo.Chauffeurs");
            DropTable("dbo.CarTypes");
            DropTable("dbo.Cars");
        }
    }
}
