namespace CarSeba.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigrations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RentRequests", "Fare", c => c.Int());
            AlterColumn("dbo.RentRequests", "ReservedCarId", c => c.Int());
            AlterColumn("dbo.RentRequests", "ChauffeurId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RentRequests", "ChauffeurId", c => c.Int(nullable: false));
            AlterColumn("dbo.RentRequests", "ReservedCarId", c => c.Int(nullable: false));
            AlterColumn("dbo.RentRequests", "Fare", c => c.Int(nullable: false));
        }
    }
}
