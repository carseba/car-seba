namespace CarSeba.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RentRequests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(nullable: false),
                        ContactNumber = c.Int(nullable: false),
                        PickUpFrom = c.String(nullable: false),
                        PickUpDate = c.DateTime(nullable: false),
                        PickUpTime = c.DateTime(nullable: false),
                        DropOffTo = c.String(nullable: false),
                        DropOffDate = c.DateTime(nullable: false),
                        DropOffTime = c.DateTime(nullable: false),
                        ServiceStatus = c.String(),
                        Fare = c.Int(nullable: false),
                        ReservedCarId = c.Int(nullable: false),
                        ChauffeurId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RentRequests");
        }
    }
}
