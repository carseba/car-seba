﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.Repository;

namespace CarSeba.WebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CarTypeController : ApiController
    {
        public CarTypeRepository Repository { get; set; }

        public CarTypeController()
        {
            Repository = new CarTypeRepository();
        }
        // GET api/cartype
        public IList<CarTypeDto> Get()
        {
            var response = Repository.GetAll();
            return response;
        }

        // GET api/cartype/5
        public CarTypeDto Get(int id)
        {
            var response = Repository.GetById(id);
            return response; 
        }

        // POST api/cartype
        public void Post([FromBody]CarType carType)
        {
            Repository.Add(carType);
        }

        // PUT api/cartype/5
        public void Put(int id, [FromBody]CarType carType)
        {
            Repository.Update(id, carType);
        }

        // DELETE api/cartype/5
        public void Delete(int id)
        {
            Repository.Remove(id);
        }
    }
}
