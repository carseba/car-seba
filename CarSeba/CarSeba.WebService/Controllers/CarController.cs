﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.Repository;

namespace CarSeba.WebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CarController : ApiController
    {
        public CarRepository Repository { get; set; }

        public CarController()
        {
            Repository = new CarRepository();
        }
        // GET api/car
        public IList<CarDto> Get()
        {
            return Repository.GetAll();
        }

        // GET api/car/5
        public CarDto Get(int id)
        {
            var response = Repository.GetById(id);
            return response;
        }

        // POST api/car
        public void Post([FromBody]Car car)
        {
            Repository.Add(car);
        }

        // PUT api/car/5
        public void Put(int id, [FromBody]Car car)
        {
            Repository.Update(id,car);
        }

        // DELETE api/car/5
        public void Delete(int id)
        {
            Repository.Remove(id);
        }
    }
}
