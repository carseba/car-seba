﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.Repository;

namespace CarSeba.WebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ChauffeurController : ApiController
    {
        public ChauffeurRepository Repository { get; set; }

        public ChauffeurController()
        {
            Repository = new ChauffeurRepository();
        }
        // GET api/chauffeur
        public IList<ChauffeurDto> Get()
        {
            var response = Repository.GetAll();
            return response;
        }

        // GET api/chauffeur/5
        public ChauffeurDto Get(int id)
        {
            var response = Repository.GetById(id);
            return response;
        }

        // POST api/chauffeur
        public void Post([FromBody]Chauffeur chauffeur)
        {
            Repository.Add(chauffeur);
        }

        // PUT api/chauffeur/5
        public void Put(int id, [FromBody]Chauffeur chauffeur)
        {
            Repository.Update(id, chauffeur);
        }

        // DELETE api/chauffeur/5
        public void Delete(int id)
        {
            Repository.Remove(id);
        }
    }
}
