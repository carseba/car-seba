﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using CarSeba.Models.DTOs;
using CarSeba.Models.Entities;
using CarSeba.Repositories.Repository;



namespace CarSeba.WebService.Controllers
{
    [EnableCors(origins:"*",headers: "*", methods: "*")]
    public class RentRequestController : ApiController
    {
        public RentRequestRepository Repository { get; set; }

        public RentRequestController()
        {
            Repository = new RentRequestRepository();
        }

        // GET api/rentrequest
        public IList<RentRequestDto> Get()
        {
            return Repository.GetAll();
        }


        // GET api/rentrequest/5
        public RentRequestDto Get(int id)
        {
            var response = Repository.GetById(id);
            return response;
        }

        // POST api/rentrequest
        public void Post([FromBody]RentRequest rentRequest)
        {
            Repository.Add(rentRequest);
        }

        // PUT api/rentrequest/5
        public void Put(int id, [FromBody]RentRequest rentRequest)
        {
            Repository.Update(id, rentRequest);
        }


        // DELETE api/rentrequest/5
        public void Delete(int id)
        {
            Repository.Remove(id);
        }

    }
}
