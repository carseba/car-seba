﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using CarSeba.Models.Entities;
using CarSeba.Repositories.Repository;

namespace CarSeba.WebService.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        public UserRepository Repository { get; set; }

        public UserController()
        {
            Repository = new UserRepository();
        }
        // GET api/user
        public IEnumerable<User> Get()
        {
            var response = Repository.GetAll();
            return response;
        }

        // GET api/user/5
        public User Get(int id)
        {
            var response = Repository.GetById(id);
            return response;
        }

        // POST api/user
        public void Post([FromBody]User user)
        {
            Repository.Add(user);
        }

        // PUT api/user/5
        public void Put(int id, [FromBody]User user)
        {
            Repository.Update(id, user);
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
            Repository.Remove(id);
        }
    }
}