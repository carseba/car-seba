﻿(function () {
    "use strict";

    angular.module("userManageModule")
        .controller("userAddCtrl", userAddCtrl);

    userAddCtrl.$inject = ["$state", "userResource"];

    function userAddCtrl($state, userResource) {

        var vm = this;

        vm.user = {
            id: null,
            userName: '',
            password: '',
            userType:''
        };

        vm.add = function (newUser) {
            userResource.create(newUser,function(data) {
                toastr.success('New User Added');
            });
            //console.log(newCarType);
            $state.go('admin.user.list');
        };

        vm.reset = function () {
            vm.user = {
                id: null,
                userName: '',
                password: '',
                userType: ''
            };
        };

        vm.userTypes = ['Admin', 'Operator'];


    };
}());