﻿(function () {
    "use strict";

    angular.module("userManageModule")
        .controller("userEditCtrl", userEditCtrl);

    userEditCtrl.$inject = ["$state", "user", "userResource"];

    function userEditCtrl($state, user, userResource) {

        var vm = this;

        vm.test = user;
        console.log(vm.test);

        vm.update = function (id, existing) {
            console.log(id);
            console.log(existing);
            userResource.update({ id: id }, existing,function(data) {
                toastr.success('User Info. Updated');
            });
            $state.go('admin.user.list');
        };
        
    };
}());