﻿(function () {
    "use strict";

    angular.module("userManageModule")
        .controller("userListCtrl", userListCtrl);

    userListCtrl.$inject = ["userResource", "users"];

    function userListCtrl(userResource, users) {

        var vm = this;
        vm.users = [];

        //userResource.query(function (data) {
        //    vm.users = data;
        //    console.log(data);
        //});

        vm.users = users;
        

        vm.remove = function (id, i) {
            console.log(id);
            userResource.remove({ id: id },function(data) {
                toastr.error('User Removed');
            });
            vm.users.splice(i, 1);
        };

    };
}());