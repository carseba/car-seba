﻿(function () {
    "use strict";

    angular.module('rentRequestModule')
           .controller("makeRequestCtrl", makeRequestCtrl);

    makeRequestCtrl.$inject = ["rentRequestResource", "$state"];

    function makeRequestCtrl(rentRequestResource, $state) {

        var vm = this;

        // data
        vm.rentRequest = {
            id: null,

            customerName: "",
            contactNumber: "",

            pickUpFrom: "",
            pickUpDate: null,
            pickUpTime: null,

            dropOffTo: "",
            dropOffDate: null,
            dropOffTime: null,

            serviceStatus: "",
            fare: null,
            reservedCarId: null,
            chauffeurId: null

        }
        

        // methods
        vm.addRequest = addRequest;
                    
        vm.reset = function () {

            vm.rentRequest = {
                id: null,

                customerName: "",
                contactNumber: "",

                pickUpFrom: "",
                pickUpDate: null,
                pickUpTime: null,

                dropOffTo: "",
                dropOffDate: null,
                dropOffTime: null,

                serviceStatus: "",
                fare: null,
                reservedCarId: null,
                chauffeurId: null

            }

        }

        function addRequest(newRequest) {

            var req1 = newRequest.pickUpDate;
            newRequest.pickUpDate = new Date(req1);
            //console.log(typeof (newRequest.pickUpDate));

            var req2 = newRequest.pickUpTime;
            newRequest.pickUpTime = new Date(req2);
            //console.log(typeof (newRequest.pickUpTime));

            var req3 = newRequest.dropOffDate;
            newRequest.dropOffDate = new Date(req3);
            //console.log(typeof (newRequest.dropOffDate));

            var req4 = newRequest.dropOffTime;
            newRequest.dropOffTime = new Date(req4);
            //console.log(typeof (newRequest.dropOffTime));
            console.log(newRequest);
            newRequest.serviceStatus = "Requested";
            rentRequestResource.save(newRequest, function (data) {
                toastr.success("Requesr Added Successfully");
            });
            vm.reset();
            $state.go('home');

        };

        // Date & Time
        vm.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.opened = !vm.opened;
        }

        vm.date = new Date("Sat Aug 29 2015 00:00:00 GMT+0600 (Azores Standard Time)");

        vm.do = function () {
            console.log(vm.date);
            console.log(typeof (vm.date));
            console.log('Time changed to: ' + vm.mytime);
        };

        //vm.mytime1 = new Date();
        vm.mytime1 = new Date();
        vm.mytime2 = new Date();

        vm.hstep = 1;
        vm.mstep = 15;

        vm.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };

        vm.ismeridian = true;

        //vm.changed1 = function () {
        //    vm.rentRequest.pickUpTime = vm.mytime1;
        //};

        vm.changed1 = function () {
            //vm.rentRequest.pickUpTime = vm.mytime;
            vm.rentRequest.pickUpTime = vm.mytime1;
            console.log(vm.rentRequest.pickUpTime);
        };

        
        vm.changed2 = function () {
            //vm.rentRequest.pickUpTime = vm.mytime;
            vm.rentRequest.dropOffTime = vm.mytime2;
            console.log(vm.rentRequest.dropOffTime);
        };
    };

}());