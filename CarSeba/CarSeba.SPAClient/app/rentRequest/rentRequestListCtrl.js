﻿(function () {
    "use strict";

    angular.module('rentRequestModule')
           .controller("rentRequestListCtrl", rentRequestListCtrl);

    rentRequestListCtrl.$inject = ["rentRequestResource"];

    function rentRequestListCtrl(rentRequestResource) {

        var vm = this;

        // data
        vm.rentRequests = rentRequestResource.query();
        console.log(vm.rentRequests);

        // methods

        // functionality 
        vm.remove = function (id, i) {
            console.log(id);
            rentRequestResource.remove({ id: id },function(data) {
                toastr.error("Rent Request Cancelled");
            });
            vm.rentRequests.splice(i, 1);
        };

        vm.currentPage = 0;
        vm.pageSize = 5;
        vm.numberOfPages = function () {

            return Math.ceil(vm.rentRequests.length / vm.pageSize);
        }

    }

}());