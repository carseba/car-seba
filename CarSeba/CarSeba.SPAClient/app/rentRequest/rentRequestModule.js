﻿(function () {
    "use strict";

    angular.module('rentRequestModule',
        [
            'common.services',
            'ui.mask'
        ]);

}());