﻿(function () {
    "use strict";

    angular.module('rentRequestModule')
           .controller("rentRequestDetailCtrl", rentRequestDetailCtrl);

    rentRequestDetailCtrl.$inject = ['$stateParams', 'rentRequestResource', 'rentRequest', 'chauffeurResource',
        'carResource', 'carTypeResource'];

    function rentRequestDetailCtrl($stateParams, rentRequestResource, rentRequest, chauffeurResource, carResource, carTypeResource) {

        var vm = this;
        vm.i = parseInt($stateParams.id);
        vm.rentRequest = rentRequest;
        //console.log(vm.rentRequest);

        vm.chauffeurName = chauffeurResource.get({ id: rentRequest.chauffeurId });
        
        vm.assignCar = carResource.get({ id: rentRequest.reservedCarId });
        console.log(vm.assignCar);

        //vm.typeCar = carTypeResource.get({ id: vm.assignCar.carTypeId });
    };

}());