﻿(function () {
    "use strict";

    angular.module('rentRequestModule')
           .controller("rentRequestEditCtrl", rentRequestEditCtrl);

    rentRequestEditCtrl.$inject = ['$stateParams', '$state', 'rentRequestResource', 'rentRequest', 'cars', 'chauffeurs'];

    function rentRequestEditCtrl($stateParams, $state, rentRequestResource, rentRequest, cars, chauffeurs) {

        var vm = this;
        vm.i = parseInt($stateParams.id);

        execute();
        
        function execute() {
            
            var req1 = rentRequest.pickUpDate;
            rentRequest.pickUpDate = new Date(req1);

            var req2 = rentRequest.pickUpTime;
            rentRequest.pickUpTime = new Date(req2);

            var req3 = rentRequest.dropOffDate;
            rentRequest.dropOffDate = new Date(req3);

            var req4 = rentRequest.dropOffTime;
            rentRequest.dropOffTime = new Date(req4);

            vm.rentRequest = rentRequest;

            vm.cars = cars;
            vm.chauffeurs = chauffeurs;

        }

        vm.statusType = ['Requested', 'Confirmed','On Hold', 'On Service', 'Serviced'];

        vm.update = function (id, existing) {
           
            var req1 = existing.pickUpDate;
            existing.pickUpDate = new Date(req1);

            var req2 = existing.pickUpTime;
            existing.pickUpTime = new Date(req2);

            var req3 = existing.dropOffDate;
            existing.dropOffDate = new Date(req3);

            var req4 = existing.dropOffTime;
            existing.dropOffTime = new Date(req4);
            console.log(existing);
            rentRequestResource.update({ id: id }, existing,function(data) {
                toastr.success("Rent Information Edited");
            });

            $state.go('rentRequest.list');
        };

        //vm.rentRequest = rentRequest;

        //var req1 = vm.rentRequest.pickUpDate;
        //vm.rentRequest.pickUpDate = new Date(req1);

        //var req2 = vm.rentRequest.pickUpTime;
        //vm.rentRequest.pickUpTime = new Date(req2);

        //var req3 = vm.rentRequest.dropOffDate;
        //vm.rentRequest.dropOffDate = new Date(req3);

        //var req4 = vm.rentRequest.dropOffTime;
        //vm.rentRequest.dropOffTime = new Date(req4);

        //console.log(typeof (vm.rentRequest.pickUpDate));
        //console.log(vm.rentRequest.pickUpDate);

        // Date & Time
        vm.open = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            vm.opened = !vm.opened;
        }

        vm.date = new Date("Sat Aug 29 2015 00:00:00 GMT+0600 (Azores Standard Time)");

        vm.do = function () {
            console.log(vm.date);
            console.log(typeof (vm.date));
            console.log('Time changed to: ' + vm.mytime);
        };

        //vm.mytime1 = new Date();
        vm.mytime1 = new Date();
        vm.mytime2 = new Date();

        vm.hstep = 1;
        vm.mstep = 15;

        vm.options = {
            hstep: [1, 2, 3],
            mstep: [1, 5, 10, 15, 25, 30]
        };

        vm.ismeridian = true;

        //vm.changed1 = function () {
        //    vm.rentRequest.pickUpTime = vm.mytime1;
        //};

        vm.changed1 = function () {
            //vm.rentRequest.pickUpTime = vm.mytime;
            vm.rentRequest.pickUpTime = vm.mytime1;
            console.log(vm.rentRequest.pickUpTime);
        };


        vm.changed2 = function () {
            //vm.rentRequest.pickUpTime = vm.mytime;
            vm.rentRequest.dropOffTime = vm.mytime2;
            console.log(vm.rentRequest.dropOffTime);
        };
    };

}());