﻿(function () {
    "use strict";

    angular.module("carManageModule")
        .controller("carDetailCtrl", carDetailCtrl);

    carDetailCtrl.$inject = ["$stateParams", "car"];

    function carDetailCtrl($stateParams, car) {

        var vm = this;
        vm.car = car;
        console.log(vm.car);
        
    };
}());