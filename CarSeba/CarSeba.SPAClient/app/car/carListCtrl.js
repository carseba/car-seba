﻿(function () {
    "use strict";

    angular.module("carManageModule")
        .controller("carListCtrl", carListCtrl);

    carListCtrl.$inject = ["carResource", "cars"];

    function carListCtrl(carResource, cars) {

        var vm = this;
        vm.cars = [];

        vm.cars = cars;
        console.log(vm.cars);
        
        vm.remove = function (id, i) {
            console.log(id);
            carResource.remove({ id: id },function(data) {
                toastr.error("Car Removed");
            });
            vm.cars.splice(i, 1);
        };

    };
}());