﻿(function () {
    "use strict";

    angular.module("carManageModule")
        .controller("carEditCtrl", carEditCtrl);

    carEditCtrl.$inject = ["$state", "carTypes", "car", "carResource"];

    function carEditCtrl($state, carTypes, car, carTypeResource) {

        var vm = this;

        vm.car = car;
        //console.log(vm.carType);
        vm.carTypes = carTypes;

        vm.update = function (id, existing) {
            console.log(id);
            console.log(existing);
            carTypeResource.update({ id: id }, existing, function(data) {
                toastr.success("Car Info. Edited");
            });
            $state.go('admin.car.list');
        };
        
    };
}());