﻿(function () {
    "use strict";

    angular.module("carTypeManageModule")
        .controller("carAddCtrl", carAddCtrl);

    carAddCtrl.$inject = ["$state", "carResource", "carTypes"];

    function carAddCtrl($state, carResource, carTypes) {

        var vm = this;

        vm.car = {
            id: null,
            registrationNo: '',
            modelName: '',
            brandName: '',
            carTypeId: null,
            rentRequests: []
        }

        vm.carTypes = carTypes;
        console.log(carTypes);

        vm.add = function (newCar) {
            carResource.create(newCar,function(data) {
                toastr.success("New Car Added");
            });
            vm.reset();
            console.log(newCar);
            $state.go('admin.car.list');
        };

        vm.reset = function () {
            vm.car = {
                id: null,
                registrationNo: '',
                modelName: '',
                brandName: '',
                carTypeId: null,
                rentRequests: []
            }
        };
        
    };
}());