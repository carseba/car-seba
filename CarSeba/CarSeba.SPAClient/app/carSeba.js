﻿(function() {
    "use strict";

    var app = angular.module('carSeba',
        [
            'common.services',
            'rentRequestModule',
            'userManageModule',
            'chauffeurManageModule',
            'carTypeManageModule',
            'carManageModule',
            'ui.router',
            //'ui.mask',
            'ui.bootstrap'
        ]);

    app.config(Config);

    Config.$inject = ['$urlRouterProvider', '$stateProvider'];

    function Config($urlRouterProvider, $stateProvider) {

        $urlRouterProvider.otherwise('/home'); // default route

        $stateProvider
            .state("home", {
                url: "/home",
                templateUrl: "app/home.html",
                controller: "homeCtrl as vm"
            })
            .state("login", {
                url: "/login",
                templateUrl: "app/login.html",
                controller: "homeCtrl as vm"
            })
            .state("makeRequest", {
                url: "/makeRequest",
                abstract: true,
                templateUrl: "app/rentRequest/makeRequest.html",
                controller: "makeRequestCtrl as vm"
            })
            .state("makeRequest.personalDetail", {
                url: "/personalDetail",
                templateUrl: "app/rentRequest/personalDetail.html"
            })
            .state("makeRequest.dateTime", {
                url: "/dateTime",
                templateUrl: "app/rentRequest/dateTime.html"
            })
                .state("rentRequest", {
                    url: "/rentRequest",
                    //abstract: true,
                    templateUrl: "app/rentRequest/rentRequest.html",
                    //controller: "rentRequestCtrl as vm"
                })
                .state("rentRequest.list", {
                    url: "/list",
                    templateUrl: "app/rentRequest/list.html",
                    controller: "rentRequestListCtrl as vm"
                })
                .state("rentRequest.detail", {
                    url: "/detail/:id",
                    templateUrl: "app/rentRequest/rentRequestDetail.html",
                    controller: "rentRequestDetailCtrl as vm",
                    resolve: {
                        rentRequestResource: "rentRequestResource",
                        rentRequest: function(rentRequestResource, $stateParams) {
                            var id = $stateParams.id;
                            return rentRequestResource.get({ id: id });
                            //return carResource.query();
                        }
                    }
                })
                .state("rentRequest.edit", {
                    url: "/edit/:id",
                    templateUrl: "app/rentRequest/rentRequestEdit.html",
                    controller: "rentRequestEditCtrl as vm",
                    resolve: {
                        rentRequestResource: "rentRequestResource",
                        carResource: "carResource",
                        chauffeurResource: "chauffeurResource",
                        rentRequest: function(rentRequestResource, $stateParams) {
                            var id = $stateParams.id;
                            //return rentRequestResource.get({ id: id });
                            var editReq = rentRequestResource.get({ id: id });

                            //var req1 = editReq.pickUpDate.toString();
                            //editReq.pickUpDate = new Date(req1);

                            //var req2 = editReq.pickUpTime.toString();
                            //editReq.pickUpTime = new Date(req2);

                            //var req3 = editReq.dropOffDate.toString();
                            //editReq.dropOffDate = new Date(req3);

                            //var req4 = editReq.dropOffTime.toString();
                            //editReq.dropOffTime = new Date(req4);

                            return editReq;
                        },
                        cars: function(carResource) {
                            var data = carResource.query();
                            return data;
                        },
                        chauffeurs: function(chauffeurResource) {
                            var data = chauffeurResource.query();
                            return data;
                        }
        }
                })
                .state("operatorHome.updateInfo", {
                    url: "/updateInfo",
                    templateUrl: "app/operator/updateInfo.html"
                })
                .state("admin", {
                    url: "/admin",
                    templateUrl: "app/admin/admin.html"
                })
                .state("admin.car", {
                    url: "/car",
                    templateUrl: "app/car/car.html"
                })
                .state("admin.car.list", {
                    url: "/list",
                    templateUrl: "app/car/list.html",
                    controller: "carListCtrl as vm",
                    resolve: {
                        carResource: "carResource",
                        cars: function (carResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carResource.query();
                        }                            
                }
                })
                .state("admin.car.add", {
                    url: "/add",
                    templateUrl: "app/car/add.html",
                    controller: "carAddCtrl as vm",
                    resolve: {
                        carTypeResource: "carTypeResource",
                        carTypes: function(carTypeResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carTypeResource.query();
                        }
                    }
                })
                .state("admin.car.detail", {
                    url: "/detail/:id",
                    templateUrl: "app/car/detail.html",
                    controller: "carDetailCtrl as vm",
                    resolve: {
                        carResource: "carResource",
                        car: function (carResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carResource.get({ id: id });
                        }
                    }
                })
                .state("admin.car.edit", {
                    url: "/edit/:id",
                    templateUrl: "app/car/edit.html",
                    controller: "carEditCtrl as vm",
                    resolve: {
                        carResource: "carResource",
                        car: function (carResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carResource.get({ id: id });
                        },
                        carTypeResource: "carTypeResource",
                        carTypes: function (carTypeResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carTypeResource.query();
                        }
                    }
                })
                .state("admin.carType", {
                    url: "/carType",
                    templateUrl: "app/carType/carType.html"
                })
                .state("admin.carType.list", {
                    url: "/list",
                    templateUrl: "app/carType/list.html",
                    controller: "carTypeListCtrl as vm",
                    resolve: {
                        carTypeResource: "carTypeResource",
                        carTypes: function (carTypeResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carTypeResource.query();
                        }
                    }
                })
                .state("admin.carType.add", {
                    url: "/add",
                    templateUrl: "app/carType/add.html",
                    controller: "carTypeAddCtrl as vm"
                })
                .state("admin.carType.detail", {
                    url: "/detail/:id",
                    templateUrl: "app/carType/detail.html",
                    controller: "carTypeDetailCtrl as vm",
                    resolve: {
                        carTypeResource: "carTypeResource",
                        carType: function (carTypeResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return carTypeResource.get({ id: id });
                        },

                    }
                })
                .state("admin.carType.edit", {
                    url: "/edit/:id",
                    templateUrl: "app/carType/edit.html",
                    controller: "carTypeEditCtrl as vm",
                    resolve: {
                        carTypeResource: "carTypeResource",
                        carType: function (carTypeResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;

                            return carTypeResource.get({ id: id });
                        }
                    }

                })
                .state("admin.user", {
                    url: "/user",
                    templateUrl: "app/user/user.html"
                })
                .state("admin.user.list", {
                    url: "/list",
                    templateUrl: "app/user/list.html",
                    controller: "userListCtrl as vm",
                    resolve: {
                        userResource: "userResource",
                        users: function (userResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return userResource.query();
                        }
                    }
                })
                .state("admin.user.add", {
                    url: "/add",
                    templateUrl: "app/user/add.html",
                    controller: "userAddCtrl as vm"
                })
                .state("admin.user.detail", {
                    url: "/detail/:id",
                    templateUrl: "app/user/detail.html",
                    controller: "userDetailCtrl as vm",
                    resolve: {
                        userResource: "userResource",
                        user: function (userResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return userResource.get({ id: id });
                        }
                    }
                })
                .state("admin.user.edit", {
                    url: "/edit/:id",
                    templateUrl: "app/user/edit.html",
                    controller: "userEditCtrl as vm",
                    resolve: {
                        userResource: "userResource",
                        user: function (userResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return userResource.get({ id: id });
                        }
                    }
                })
                .state("admin.chauffeur", {
                    url: "/chauffeur",
                    templateUrl: "app/chauffeur/chauffeur.html"
                })
                .state("admin.chauffeur.list", {
                    url: "/list",
                    templateUrl: "app/chauffeur/list.html",
                    controller: "chauffeurListCtrl as vm",
                    resolve: {
                        userResource: "chauffeurResource",
                        chauffeur: function (chauffeurResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return chauffeurResource.query();
                        }
                    }
                })
                .state("admin.chauffeur.add", {
                    url: "/add",
                    templateUrl: "app/chauffeur/add.html",
                    controller: "chauffeurAddCtrl as vm",
                })
                .state("admin.chauffeur.detail", {
                    url: "/detail/:id",
                    templateUrl: "app/chauffeur/detail.html",
                    controller: "chauffeurDetailCtrl as vm",
                    resolve: {
                        chauffeurResource: "chauffeurResource",
                        chauffeur: function (chauffeurResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return chauffeurResource.get({ id: id });
                        }
                    }

                })
                .state("admin.chauffeur.edit", {
                    url: "/edit/:id",
                    templateUrl: "app/chauffeur/edit.html",
                    controller: "chauffeurEditCtrl as vm",
                    resolve: {
                        chauffeurResource: "chauffeurResource",
                        chauffeur: function (chauffeurResource, $stateParams) {
                            var id = $stateParams.id;
                            //return carTypeResource.get({ id: id }).$promise;
                            return chauffeurResource.get({ id: id });
                        }
                    }
                });


    }

}());