﻿(function () {
    "use strict";

    angular.module('carSeba')
    .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$state'];

    function homeCtrl($state) {
        var vm = this;

        vm.myInterval = 5000;
        var slides = vm.slides = [];


        slides.push({
            image: 'assets/img/sliders/2.jpg'
        });

        vm.addSlide = function () {
            var newWidth = 600 + slides.length + 1;

        };
        for (var i = 0; i < 4; i++) {
            vm.addSlide();
        }

        vm.user = {
            userName: '',
            password: ''
        };

        vm.makeLogin = function (user) {

            if (user.userName == 'admin' && user.password == 123) {
                toastr.success("Admin Logged In");
                $state.go('admin.car.list');
            }
            else if (user.userName == 'operator' && user.password == 123) {
                toastr.success("Operator Logged In");
                $state.go('rentRequest.list');
            } else {
                toastr.warning("Please Provide Correct Data");
                $state.go('login');
            }
        }

    }

}());

