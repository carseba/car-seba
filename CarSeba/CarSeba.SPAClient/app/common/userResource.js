﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("userResource", userResource);

    userResource.$inject = ["$resource"];

    function userResource($resource) {

        return $resource('http://localhost:1554/api/user/:id', {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET" },
            remove: { method: "DELETE" },
            update: { method: "PUT" }
        });
    }

}());