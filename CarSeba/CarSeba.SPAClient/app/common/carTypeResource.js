﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("carTypeResource", carTypeResource);

    carTypeResource.$inject = ["$resource"];

    function carTypeResource($resource) {

        return $resource('http://localhost:1554/api/cartype/:id', {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET" },
            remove: { method: "DELETE" },
            update: { method: "PUT" }
        });
    }

}());