﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("chauffeurResource", chauffeurResource);

    chauffeurResource.$inject = ["$resource"];

    function chauffeurResource($resource) {

        return $resource('http://localhost:1554/api/chauffeur/:id', {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET" },
            remove: { method: "DELETE" },
            update: { method: "PUT" }
        });
    }

}());