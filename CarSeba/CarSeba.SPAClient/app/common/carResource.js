﻿(function () {
    "use strict";

    angular.module("common.services")
           .factory("carResource", carResource);

    carResource.$inject = ["$resource"];

    function carResource($resource) {

        return $resource('http://localhost:1554/api/car/:id', {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET" },
            remove: { method: "DELETE" },
            update: { method: "PUT" }
        });
    }

}());