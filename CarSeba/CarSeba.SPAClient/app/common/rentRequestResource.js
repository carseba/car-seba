﻿(function () {
    "use strict";

    angular.module("common.services")
        .factory("rentRequestResource", rentRequestResource);

    rentRequestResource.$inject = ['$resource'];

    function rentRequestResource($resource) {

        return $resource('http://localhost:1554/api/rentrequest/:id', {}, {
            query: { method: "GET", isArray: true },
            create: { method: "POST" },
            get: { method: "GET" },
            remove: { method: "DELETE" },
            update: { method: "PUT" }
        });

    };

}());