﻿(function () {
    "use strict";

    angular.module("chauffeurManageModule")
        .controller("chauffeurEditCtrl", chauffeurEditCtrl);

    chauffeurEditCtrl.$inject = ["$state", "chauffeur", "chauffeurResource"];

    function chauffeurEditCtrl($state, chauffeur, chauffeurResource) {

        var vm = this;

        vm.chauffeur = chauffeur;
        console.log(vm.chauffeur);

        vm.statusOptions = ['Available', 'On Duty', 'On Leave'];

        vm.update = function (id, existing) {
            console.log(id);
            console.log(existing);
            chauffeurResource.update({ id: id }, existing, function(data) {
                toastr.success('Chauffeur Info. Edited');
            });
            $state.go('admin.chauffeur.list');
        };
        
    };
}());