﻿(function () {
    "use strict";

    angular.module("chauffeurManageModule")
        .controller("chauffeurListCtrl", chauffeurListCtrl);

    chauffeurListCtrl.$inject = ["chauffeurResource", "chauffeur"];

    function chauffeurListCtrl(chauffeurResource, chauffeur) {

        var vm = this;
        vm.chauffeurs = [];

        vm.chauffeurs = chauffeur;
        console.log(vm.chauffeurs);

        vm.remove = function (id, i) {
            console.log(id);
            chauffeurResource.remove({ id: id },function(data) {
                toastr.error('Chauffeur Removed');
            });
            vm.chauffeurs.splice(i, 1);
        };

    };
}());