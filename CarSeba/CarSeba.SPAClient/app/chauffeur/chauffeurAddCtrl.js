﻿(function () {
    "use strict";

    angular.module("chauffeurManageModule")
        .controller("chauffeurAddCtrl", chauffeurAddCtrl);

    chauffeurAddCtrl.$inject = ["$state", "chauffeurResource"];

    function chauffeurAddCtrl($state, chauffeurResource) {

        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string LicenseNum { get; set; }
        //public string Status { get; set; }
        //public virtual ICollection<RentRequest> RentRequests { get; set; }
        var vm = this;

        vm.chauffeur = {
            id: null,
            name: '',
            licenseNum: '',
            status: '',
            rentRequests:[]
        };

        vm.add = function (newChauffeur) {
            chauffeurResource.create(newChauffeur,function(data) {
                toastr.success('New Chauffeur Added');
            });
            //console.log(newCarType);
            $state.go('admin.chauffeur.list');
        };

        vm.reset = function () {
            vm.chauffeur = {
                id: null,
                name: '',
                licenseNum: '',
                status: '',
                rentRequests: []
            };
        };

        vm.statusOptions = ['Available', 'On Duty', 'On Leave'];


    };
}());