﻿(function () {
    "use strict";

    angular.module("carTypeManageModule")
        .controller("carTypeAddCtrl", carTypeAddCtrl);

    carTypeAddCtrl.$inject = ["$state", "carTypeResource"];

    function carTypeAddCtrl($state, carTypeResource) {

        var vm = this;

        vm.carType = {
            id: null,
            name: '',
            cars:[]
        };

        vm.add = function (newCarType) {
            carTypeResource.create(newCarType,function(data) {
                toastr.success('New Car Type Added');
            });
            //console.log(newCarType);
            $state.go('admin.carType.list');
        };

        vm.reset = function () {
            vm.carType = {
                id: null,
                name: '',
                cars: []
            };
        };
        
    };
}());