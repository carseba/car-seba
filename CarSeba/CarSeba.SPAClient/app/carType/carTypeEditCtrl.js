﻿(function () {
    "use strict";

    angular.module("carTypeManageModule")
        .controller("carTypeEditCtrl", carTypeEditCtrl);

    carTypeEditCtrl.$inject = ["$state", "carType", "carTypeResource"];

    function carTypeEditCtrl($state, carType, carTypeResource) {

        var vm = this;

        vm.carType = carType;
        //console.log(vm.carType);

        vm.update = function (id, existing) {
            console.log(id);
            console.log(existing);
            carTypeResource.update({ id: id }, existing, function(data) {
                toastr.success('Car Type Info. Edited');
            });
            $state.go('admin.carType.list');
        };
        
    };
}());