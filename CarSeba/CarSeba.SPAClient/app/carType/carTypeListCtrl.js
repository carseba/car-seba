﻿(function () {
    "use strict";

    angular.module("carTypeManageModule")
        .controller("carTypeListCtrl", carTypeListCtrl);

    carTypeListCtrl.$inject = ["carTypeResource", "carTypes"];

    function carTypeListCtrl(carTypeResource, carTypes) {

        var vm = this;
        vm.carTypes = [];

        vm.carTypes = carTypes;
        console.log(vm.carTypes);
        
        vm.remove = function (id, i) {
            console.log(id);
            carTypeResource.remove({ id: id },function(data) {
                toastr.error('Car Type Removed');
            });
            vm.carTypes.splice(i, 1);
        };

    };
}());